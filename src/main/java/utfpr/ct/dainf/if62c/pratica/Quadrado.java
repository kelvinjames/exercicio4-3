/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author kelvin
 */
public class Quadrado implements FiguraComLados {
    double lado;
    public Quadrado(){
        
    }
    public Quadrado(double lado){
       this.lado = lado; 
    }
    public double getArea(){
        double area;
        area = this.lado * this.lado;
        return area;
    }
    public double getPerimetro(){
        double perimetro;
        perimetro = 4 * this.lado;
        return perimetro;
    }
    public double getLadoMaior(){
        return this.lado;
    }
    public double getLadoMenor(){
        return this.lado;
    }
}
